import * as d3 from "d3";

function error_bar(scale_x, scale_y, data) {
    const path = d3.path();
    // Vertical line
    path.moveTo(scale_x(data.x) + scale_x.bandwidth() / 2, scale_y(data.y - data.sd));
    path.lineTo(scale_x(data.x) + scale_x.bandwidth() / 2, scale_y(data.y + data.sd));
    // Bottom error bar
    path.moveTo(scale_x(data.x), scale_y(data.y - data.sd));
    path.lineTo(scale_x(data.x) + scale_x.bandwidth(), scale_y(data.y - data.sd));
    // Top error bar
    path.moveTo(scale_x(data.x), scale_y(data.y + data.sd));
    path.lineTo(scale_x(data.x) + scale_x.bandwidth(), scale_y(data.y + data.sd));
    return path.toString();
}

function error_bar_horizontal(scale_x, scale_y, data) {
    const path = d3.path();
    // Horizontal line
    path.moveTo(scale_x(data.time - data.sd), scale_y(data.name) + scale_y.bandwidth() / 2);
    path.lineTo(scale_x(data.time + data.sd), scale_y(data.name) + scale_y.bandwidth() / 2);
    // Bottom error bar (left)
    path.moveTo(scale_x(data.time - data.sd), scale_y(data.name));
    path.lineTo(scale_x(data.time - data.sd), scale_y(data.name) + scale_y.bandwidth());
    // Top error bar (right)
    path.moveTo(scale_x(data.time + data.sd), scale_y(data.name));
    path.lineTo(scale_x(data.time + data.sd), scale_y(data.name) + scale_y.bandwidth());
    return path.toString();
}

function colour_weak(dim, d) {
    return dim.colours[d.paradigm === "imperative" ? 2 : 0];
}

function colour_strong(dim, d) {
    return dim.colours[d.paradigm === "imperative" ? 3 : 1];
}

export function bar_plot_numeric(svg_parent, dim, data, label) {
    // zeroed data for initialising animations
    const zero = data.map(a => {
        return {
            x: a.x,
            y: 0,
            sd: 0,
        };
    });

    // size svg
    const svg = svg_parent
        .attr("width", dim.outer.width)
        .attr("height", dim.outer.height)
        .append("g")
        .attr("transform", `translate(${dim.margin.left},${dim.margin.top})`);

    // x scale
    const x = d3.scaleBand()
        .domain(data.map(i => i.x))
        .range([0, dim.inner_width()])
        .padding(0.2);

    // y scale
    const y = d3.scaleLinear()
        .domain([0, d3.max(data.map(i => i.y + i.sd))])
        .range([dim.inner_height(), 0])
        .nice();

    // x axis
    const x_axis = d3.axisBottom(x)
        .tickFormat(d3.format("~e"));

    svg.append("g")
        .attr("transform", `translate(0,${dim.inner_height()})`)
        .call(x_axis);

    // y axis
    svg.append("g")
        .call(d3.axisLeft(y));

    // x axes label
    svg.append("text")
        .attr("font-size", dim.font_size.axis_label)
        .attr("text-anchor", "middle")
        .attr("x", dim.inner_width() / 2)
        .attr("y", dim.margin.top + dim.inner_height() + dim.font_size.axis_label)
        .text(label.axis.x);

    // y axes label
    svg.append("text")
        .attr("font-size", dim.font_size.axis_label)
        .attr("text-anchor", "middle")
        .attr("transform", "rotate(-90)")
        .attr("y", -dim.margin.left + dim.font_size.axis_label)
        .attr("x", -dim.margin.top - dim.inner_height() / 2)
        .text(label.axis.y);

    // title
    svg.append("text")
        .attr("font-size", dim.font_size.title)
        .attr("text-anchor", "middle")
        .attr("x", -dim.margin.left + dim.outer.width / 2)
        .attr("y", -dim.margin.top + dim.font_size.title)
        .text(label.title);

    // fill data but with zero height at beginning
    svg.selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", d => x(d.x))
        .attr("width", x.bandwidth())
        .attr("fill", d => colour_weak(dim, d))
        .attr("height", dim.inner_height() - y(0))
        .attr("y", y(0));

    // animate
    svg.selectAll(".bar")
        .transition()
        .duration(dim.animate.delay.slow)
        .attr("y", d => y(d.y))
        .attr("height", d => dim.inner_height() - y(d.y))
        .delay((_, id) => id * dim.animate.progressive.slow);

    // initialise error bars
    svg.selectAll(".error")
        .data(zero)
        .enter()
        .append("path")
        .attr("class", "error")
        .attr("d", d => error_bar(x, y, d))
        .attr("fill", "none")
        .attr("stroke", d => colour_strong(dim, d))
        .attr("stroke-width", dim.line.width);

    // animate error bars
    svg.selectAll(".error")
        .data(data)
        .transition()
        .duration(dim.animate.delay.slow)
        .attr("d", d => error_bar(x, y, d))
        .delay((_, id) => id * dim.animate.progressive.slow);

}

export function bar_plot_categoric(svg_parent, dim, data, label) {
    // zeroed data for initialising animations
    const zero = data.map(a => {
        return {
            x: a.x,
            y: 0,
            sd: 0,
        };
    });

    // size svg
    const svg = svg_parent
        .attr("width", dim.outer.width)
        .attr("height", dim.outer.height)
        .append("g")
        .attr("transform", `translate(${dim.margin.left},${dim.margin.top})`);

    // x scale
    const x = d3.scaleBand()
        .domain(data.map(i => i.x))
        .range([0, dim.inner_width()])
        .padding(0.2);

    // y scale
    const y = d3.scaleLinear()
        .domain([0, d3.max(data.map(i => i.y + i.sd))])
        .range([dim.inner_height(), 0])
        .nice();

    // x axis
    const x_axis = d3.axisBottom(x);

    svg.append("g")
        .attr("transform", `translate(0,${dim.inner_height()})`)
        .call(x_axis);

    // y axis
    svg.append("g")
        .call(d3.axisLeft(y));

    // x axes label
    svg.append("text")
        .attr("font-size", dim.font_size.label)
        .attr("text-anchor", "middle")
        .attr("x", dim.inner_width() / 2)
        .attr("y", dim.margin.top + dim.inner_height() + dim.font_size.axis_label)
        .text(label.axis.x);

    // y axes label
    svg.append("text")
        .attr("font-size", dim.font_size.label)
        .attr("text-anchor", "middle")
        .attr("transform", "rotate(-90)")
        .attr("y", -dim.margin.left + dim.font_size.axis_label)
        .attr("x", -dim.margin.top - dim.inner_height() / 2)
        .text(label.axis.y);

    // title
    svg.append("text")
        .attr("font-size", dim.font_size.title)
        .attr("text-anchor", "middle")
        .attr("x", -dim.margin.left + dim.outer.width / 2)
        .attr("y", -dim.margin.top + dim.font_size.title)
        .text(label.title);

    // fill data but with zero height at beginning
    svg.selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", d => x(d.x))
        .attr("width", x.bandwidth())
        .attr("fill", d => colour_weak(dim, d))
        .attr("height", dim.inner_height() - y(0))
        .attr("y", y(0));

    // animate
    svg.selectAll(".bar")
        .transition()
        .duration(dim.animate.delay.slow)
        .attr("y", d => y(d.y))
        .attr("height", d => dim.inner_height() - y(d.y))
        .delay((_, id) => id * dim.animate.progressive.slow);

    // initialise error bars
    svg.selectAll(".error")
        .data(zero)
        .enter()
        .append("path")
        .attr("class", "error")
        .attr("d", d => error_bar(x, y, d))
        .attr("fill", "none")
        .attr("stroke", d => colour_strong(dim, d))
        .attr("stroke-width", dim.line.width);

    // animate error bars
    svg.selectAll(".error")
        .data(data)
        .transition()
        .duration(dim.animate.delay.slow)
        .attr("d", d => error_bar(x, y, d))
        .delay((_, id) => id * dim.animate.progressive.slow);
}

export function bar_plot_horizontal(svg_parent, dim, data, label) {
    // zeroed data for initialising animations
    const zero = data.map(a => {
        return {
            name: a.name,
            time: 0,
            sd: 0,
            paradigm: a.paradigm,
        };
    });

    // size svg
    const svg = svg_parent
        .attr("width", dim.outer.width)
        .attr("height", dim.outer.height)
        .append("g")
        .attr("transform", `translate(${dim.margin.left},${dim.margin.top})`);

    // x scale
    const x = d3.scaleLinear()
        .domain([0, d3.max(data.map(i => i.time + i.sd))])
        .range([0, dim.inner_width()])
        .nice();

    // y scale
    const y = d3.scaleBand()
        .domain(data.map(i => i.name))
        .range([dim.inner_height(), 0])
        .padding(0.2);

    // x axis
    const x_axis = svg.append("g")
        .attr("id", "x_axis")
        .attr("transform", `translate(0,${dim.inner_height()})`)
        .call(d3.axisBottom(x));

    // y axis
    const y_axis = svg.append("g")
        .attr("id", "y_axis")
        .call(d3.axisLeft(y));

    // x axis label
    svg.append("text")
        .attr("font-size", dim.font_size.axis_label)
        .attr("text-anchor", "middle")
        .attr("x", dim.inner_width() / 2)
        .attr("y", dim.margin.top + dim.inner_height() + dim.font_size.axis_label)
        .text(label.axis.x);

    // title
    svg.append("text")
        .attr("font-size", dim.font_size.title)
        .attr("text-anchor", "middle")
        .attr("x", -dim.margin.left + dim.outer.width / 2)
        .attr("y", -dim.margin.top + dim.font_size.title)
        .text(label.title);

    // legend
    svg.append("text")
        .attr("font-size", dim.font_size.axis_label)
        .attr("text-anchor", "end")
        .attr("x", dim.inner_width())
        .attr("y", dim.font_size.axis_label)
        .attr("fill", dim.colours[3])
        .text("Imperative");

    svg.append("text")
        .attr("font-size", dim.font_size.axis_label)
        .attr("text-anchor", "end")
        .attr("x", dim.inner_width())
        .attr("y", 2 * dim.font_size.axis_label + 10)
        .attr("fill", dim.colours[1])
        .text("Declarative");

    // // tooltip
    // const tooltip = svg
    //     .append("div")
    //     .style("opacity", 0)
    //     .attr("class", "tooltip")
    //     .style("background-color", "white")
    //     .style("border", "solid")
    //     .style("border-width", "2px")
    //     .style("border-radius", "5px")
    //     .style("padding", "5px");

    const mouseover = function(d) {
        // tooltip.style("opacity", 1);
        d3.select(this)
            .style("stroke", "black");
    }

    // const mousemove = function(d) {
    //     console.log(d3.pointer(this));
    //     tooltip
    //         .html(`${d.name} took ${d.time}s`)
    //         .style("left", (d3.pointer(this)[0] + 70) + "px")
    //         .style("top", (d3.pointer(this)[1]) + "px");
    // }

    const mouseleave = function(d) {
        // tooltip.style("opacity", 0);
        d3.select(this)
            .style("stroke", "none");
    }

    // fill data but with zero width at beginning
    svg.selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", x(0))
        .attr("width", x(0))
        .attr("y", d => y(d.name))
        .attr("height", y.bandwidth())
        .attr("fill", d => colour_weak(dim, d))
        .on("mouseover", mouseover)
        // .on("mousemove", mousemove)
        .on("mouseleave", mouseleave);

    // animate
    svg.selectAll(".bar")
        .transition()
        .duration(dim.animate.delay.fast)
        .attr("width", d => x(d.time))
        .delay((_, id) => id * dim.animate.progressive.fast);

    // initialise error bars
    svg.selectAll(".error")
        .data(zero)
        .enter()
        .append("path")
        .attr("class", "error")
        .attr("d", d => error_bar_horizontal(x, y, d))
        .attr("fill", "none")
        .attr("stroke", d => colour_strong(dim, d))
        .attr("stroke-width", dim.line.width);

    // animate error bars
    svg.selectAll(".error")
        .data(data)
        .transition()
        .duration(dim.animate.delay.fast)
        .attr("d", d => error_bar_horizontal(x, y, d))
        .delay((_, id) => id * dim.animate.progressive.fast);

    return [svg, x, y];
}

export function update_bar_plot_horizontal(svg, x, y, dim, data) {
    // x scale
    x.domain([0, d3.max(data.map(i => i.time + i.sd))]).nice();

    // y scale
    y.domain(data.map(i => i.name));

    // x axis
    svg.select("#x_axis")
        .transition()
        .duration(dim.animate.delay.slow)
        .call(d3.axisBottom(x));

    // y axis
    svg.select("#y_axis")
        .transition()
        .duration(dim.animate.delay.slow)
        .call(d3.axisLeft(y));

    // data
    svg.selectAll(".bar")
        .data(data)
        .join(
            enter => enter
            .append("rect")
            .attr("class", "bar"),
            update => update,
            exit => exit
            .transition()
            .duration(dim.animate.delay.slow)
            .delay((_, id) => id * dim.animate.progressive.fast)
            .attr("width", x(0))
            .remove(),
        )
        .transition()
        .duration(dim.animate.delay.slow)
        .attr("width", d => x(d.time))
        .attr("y", d => y(d.name))
        .attr("height", y.bandwidth())
        .delay((_, id) => id * dim.animate.progressive.fast)
        .attr("fill", d => colour_weak(dim, d));

    // error bars
    svg.selectAll(".error")
        .data(data)
        .join(
            enter => enter
            .append("path")
            .style("opacity", 0)
            .attr("class", "error")
            .attr("stroke-width", dim.line.width),
            update => update,
            exit => exit
            .transition()
            .style("opacity", 0)
            .remove(),
        )
        .transition()
        .duration(dim.animate.delay.slow)
        .attr("d", d => error_bar_horizontal(x, y, d))
        .delay((_, id) => id * dim.animate.progressive.fast)
        .attr("stroke", d => colour_strong(dim, d))
        .style("opacity", 1);
}
