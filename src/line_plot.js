import * as d3 from "d3";

function update_line(scale_x, scale_y, svg, nested, dim, colours) {
    nested.forEach((v, k) => {
        svg.selectAll(`.plot_line_${k}`)
            .data([v])
            .join(
                (enter) =>
                    enter
                        .append("path")
                        .attr("class", `plot_line_${k}`)
                        .attr("fill", "none")
                        .attr("stroke", colours(k))
                        .attr("stroke-width", dim.line.width)
                        .style("opacity", 0),
                (update) => update,
                (exit) =>
                    exit
                        .transition()
                        .delay((_, id) => id * dim.animate.progressive.fast)
                        .duration(dim.animate.delay.fast)
                        .style("opacity", 0)
                        .remove()
            )
            .transition()
            .delay((_, id) => id * dim.animate.progressive.fast)
            .duration(dim.animate.delay.slow)
            .style("opacity", 1)
            .attr(
                "d",
                d3
                    .line()
                    .x((d) => scale_x(d.x))
                    .y((d) => scale_y(d.y))
            );
    });
}

function update_error(scale_x, scale_y, svg, nested, dim, colours) {
    nested.forEach((v, k) => {
        svg.selectAll(`.error_${k}`)
            .data([v])
            .join(
                (enter) =>
                    enter
                        .append("path")
                        .attr("class", `error_${k}`)
                        .attr("fill", colours(k))
                        .attr("stroke", "none")
                        .style("opacity", 0),
                (update) => update,
                (exit) =>
                    exit
                        .transition()
                        .delay((_, id) => id * dim.animate.progressive.fast)
                        .duration(dim.animate.delay.fast)
                        .style("opacity", 0)
                        .remove()
            )
            .transition()
            .delay((_, id) => id * dim.animate.progressive.fast)
            .duration(dim.animate.delay.slow)
            .style("opacity", 0.5)
            .attr(
                "d",
                d3
                    .area()
                    .x((d) => scale_x(d.x))
                    .y0((d) => scale_y(d.y - d.sd))
                    .y1((d) => scale_y(d.y + d.sd))
            );
    });
}

function update_dots(scale_x, scale_y, svg, nested, dim, colours) {
    nested.forEach((v, k) => {
        svg.selectAll(`.dot_${k}`)
            .data(v)
            .join(
                (enter) =>
                    enter
                        .append("circle")
                        .attr("class", `dot_${k}`)
                        .attr("r", dim.point.radius)
                        .attr("fill", colours(k))
                        .attr("cx", (d) => scale_x(d.x))
                        .attr("cy", (d) => scale_y(d.y))
                        .style("opacity", 0),
                (update) => update,
                (exit) =>
                    exit
                        .transition()
                        .delay((_, id) => id * dim.animate.progressive.fast)
                        .duration(dim.animate.delay.fast)
                        .style("opacity", 0)
                        .remove()
            )
            .transition()
            .delay((_, id) => id * dim.animate.progressive.fast)
            .duration(dim.animate.delay.slow)
            .style("opacity", 1)
            .attr("cy", (d) => scale_y(d.y));
    });
}

export function update_all(scale_x, scale_y, svg, data, dim) {
    let nested = d3.group(data, (d) => d.name);

    if (nested.has("declarative")) {
        svg.append("text")
            .attr("font-size", dim.font_size.axis_label)
            .attr("text-anchor", "start")
            .attr("x", 10)
            .attr("y", 2 * dim.font_size.axis_label + 10)
            .attr("fill", dim.colours[1])
            .text("Declarative");
    } else {
        nested.set("declarative", "");
    }

    if (nested.has("imperative")) {
        svg.append("text")
            .attr("font-size", dim.font_size.axis_label)
            .attr("text-anchor", "start")
            .attr("x", 10)
            .attr("y", dim.font_size.axis_label)
            .attr("fill", dim.colours[3])
            .text("Imperative");
    } else {
        nested.set("imperative", "");
    }

    let names = [...nested.keys()].reverse();

    const colours_strong = d3
        .scaleOrdinal()
        .domain(names)
        .range(dim.colours.filter((v, i) => i % 2 === 1));

    const colours_weak = d3
        .scaleOrdinal()
        .domain(names)
        .range(dim.colours.filter((v, i) => i % 2 === 0));

    update_error(scale_x, scale_y, svg, nested, dim, colours_weak);
    update_line(scale_x, scale_y, svg, nested, dim, colours_strong);
    update_dots(scale_x, scale_y, svg, nested, dim, colours_strong);
}

export function set_lin(scale_x, svg, data, dim) {
    // y scale
    const y = d3
        .scaleLinear()
        .domain([0, d3.max(data.map((i) => i.y + i.sd))])
        .range([dim.inner_height(), 0])
        .nice();

    // y axis
    svg.select("#y_axis")
        .transition()
        .duration(dim.animate.delay.slow)
        .call(d3.axisLeft(y));

    return y;
}

export function set_log(scale_x, svg, data, dim) {
    // y scale
    const y = d3
        .scaleLog()
        .domain([
            d3.min(data.map((i) => i.y - i.sd)),
            d3.max(data.map((i) => i.y + i.sd)),
        ])
        .range([dim.inner_height(), 0])
        .nice();

    // y axis
    svg.select("#y_axis")
        .transition()
        .duration(dim.animate.delay.slow)
        .call(d3.axisLeft(y));

    return y;
}

export function line_plot(svg_parent, dim, data, label) {
    // zeroed data for initialising animations
    const zero = data.map((a) => ({
        name: a.name,
        x: a.x,
        y: 0,
        sd: 0,
    }));

    // size svg
    const svg = svg_parent
        .attr("width", dim.outer.width)
        .attr("height", dim.outer.height)
        .append("g")
        .attr("transform", `translate(${dim.margin.left},${dim.margin.top})`);

    // x scale
    const x = d3
        .scaleLog()
        .domain(d3.extent(data.map((i) => i.x)))
        .range([0, dim.inner_width()])
        .nice();

    // x axis
    svg.append("g")
        .attr("id", "x_axis")
        .attr("transform", `translate(0,${dim.inner_height()})`)
        .call(d3.axisBottom(x).ticks(data.length, "~e"));

    // y scale and axis
    svg.append("g").attr("id", "y_axis");
    const y = set_lin(x, svg, data, dim);

    // x axis label
    svg.append("text")
        .attr("font-size", dim.font_size.axis_label)
        .attr("text-anchor", "middle")
        .attr("x", dim.inner_width() / 2)
        .attr(
            "y",
            dim.margin.top + dim.inner_height() + dim.font_size.axis_label
        )
        .text(label.axis.x);

    // y axis label
    svg.append("text")
        .attr("font-size", dim.font_size.axis_label)
        .attr("text-anchor", "middle")
        .attr("transform", "rotate(-90)")
        .attr("y", -dim.margin.left + dim.font_size.axis_label)
        .attr("x", -dim.margin.top - dim.inner_height() / 2)
        .text(label.axis.y);

    // title
    svg.append("text")
        .attr("font-size", dim.font_size.title)
        .attr("text-anchor", "middle")
        .attr("x", -dim.margin.left + dim.outer.width / 2)
        .attr("y", -dim.margin.top + dim.font_size.title)
        .text(label.title);

    // initialise lines and dots on x axis
    update_all(x, y, svg, zero, dim);

    // animate
    update_all(x, y, svg, data, dim);

    return [x, y, svg];
}
