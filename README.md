# [datavis](https://gitlab.com/eidoom/datavis)
* Live [here](https://eidoom.gitlab.io/datavis/)
* [Companion post](https://computing-blog.netlify.app/post/datavis/)

## Data
* Generate over at [pi/leibniz](https://gitlab.com/eidoom/pi/-/tree/master/leibniz)

## Technology
* [d3](https://d3js.org/)
* [rollup](https://rollupjs.org)

## Resources
* <https://github.com/d3/d3/blob/master/API.md>
* <https://www.d3-graph-gallery.com/index.html>

## Getting started
```shell
npm install
npm run build
```

## TODO
* Highlighting on mouseover https://www.d3-graph-gallery.com/graph/barplot_stacked_highlight.html
* Tooltips https://www.d3-graph-gallery.com/graph/interactivity_tooltip.html#template
* Zoom/pan/brush https://www.d3-graph-gallery.com/graph/interactivity_brush.html#brushingforzoom
