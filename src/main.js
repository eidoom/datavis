"use strict";

import * as d3 from "d3";
import { line_plot, set_log, set_lin, update_all } from "./line_plot.js";
import { bar_plot_horizontal, update_bar_plot_horizontal } from "./bar_plot.js";

function capitalise(string) {
    return string.replace(/\w/, (c) => c.toUpperCase());
}

class Dim {
    constructor() {
        this.outer = {
            width: 640,
            height: 480,
        };
        this.margin = {
            top: 30,
            right: 20,
            bottom: 50,
            left: 60,
        };
        this.font_size = {
            axis_label: 14,
            title: 16,
        };
        this.line = {
            width: 1.5,
        };
        this.point = {
            radius: 4,
        };
        this.animate = {
            delay: {
                fast: 500,
                slow: 1000,
            },
            progressive: {
                fast: 3,
                slow: 100,
            },
        };
        this.colours = d3.schemePaired.slice(2);
    }

    inner_width() {
        return this.outer.width - this.margin.left - this.margin.right;
    }

    inner_height() {
        return this.outer.height - this.margin.top - this.margin.bottom;
    }
}

function all_times(data, a, first) {
    return data.languages
        .map((language) =>
            language.implementations.map((implementation) =>
                implementation.paradigms.map((paradigm) => {
                    let v;
                    if (implementation.interpreter) {
                        v = implementation.interpreter.version;
                    } else {
                        v = implementation.compiler.version;
                    }
                    return {
                        name: `${capitalise(language.name)} (${
                            implementation.name
                        } ${v}) ${paradigm.styles[0].slice(0, 3)}.`,
                        time: d3.mean(paradigm.bench[a].times),
                        sd: d3.deviation(paradigm.bench[a].times),
                        paradigm: paradigm.styles[0],
                        version: v,
                    };
                })
            )
        )
        .flat(2)
        .sort((a, b) => a.time - b.time)
        .slice(0, first)
        .reverse();
}

function single_times(data, a) {
    const aa = data.languages
        .map((lang) => lang.implementations)
        .flat()
        [a].paradigms.map((paradigm) =>
            paradigm.bench.map((b) => {
                return {
                    name: paradigm.styles[0],
                    x: b.n,
                    y: d3.mean(b.times),
                    sd: d3.deviation(b.times),
                };
            })
        )
        .flat();
    return aa;
}

d3.json("data/results.json").then((data) => {
    {
        const cntr = d3
            .select(".container")
            .append("div")
            .attr("class", "opts-cntr");

        const svg_lin_parent = cntr.append("svg");

        const opts = cntr.append("div").attr("class", "opts");

        let log = false;

        const radio1 = opts.append("div");

        const input1 = radio1
            .append("input")
            .attr("type", "radio")
            .attr("id", `radio1`)
            .attr("name", "log_y_scale")
            .attr("value", true);

        radio1.append("label").attr("for", `radio1`).text("Log");

        input1.on("click", () => {
            log = true;
            y = set_log(x, svg_lin, zip, dim);
            update_all(x, y, svg_lin, zip, dim);
        });

        const radio2 = opts.append("div");

        const input2 = radio2
            .append("input")
            .attr("type", "radio")
            .attr("id", `radio2`)
            .attr("name", "log_y_scale")
            .attr("value", false)
            .property("checked", "true");

        radio2.append("label").attr("for", `radio2`).text("Lin");

        input2.on("click", () => {
            log = false;
            y = set_lin(x, svg_lin, zip, dim);
            update_all(x, y, svg_lin, zip, dim);
        });

        let impls = data.languages
            .map((lang) =>
                lang.implementations.map(
                    (impl) => `${lang.name} (${impl.name})`
                )
            )
            .flat();

        let choice = 0;

        const slct = opts.append("select");

        slct.selectAll()
            .data(
                impls.map((impl, i) => {
                    return {
                        n: i,
                        lang: impl,
                    };
                })
            )
            .enter()
            .append("option")
            .text((d) => capitalise(d.lang))
            .attr("value", (d) => d.n);

        slct.on("change", function (d) {
            choice = d3.select(this).property("value");
            zip = single_times(data, choice);
            if (log) {
                y = set_log(x, svg_lin, zip, dim);
            } else {
                y = set_lin(x, svg_lin, zip, dim);
            }
            update_all(x, y, svg_lin, zip, dim);
        });

        let zip = single_times(data, choice);

        const labels = {
            axis: {
                x: "Number of terms in series",
                y: "Time (s)",
            },
            title: "Growth rates",
        };

        const dim = new Dim();

        let [x, y, svg_lin] = line_plot(svg_lin_parent, dim, zip, labels);
    }
    {
        const cntr = d3
            .select(".container")
            .append("div")
            .attr("class", "opts-cntr");

        const svg_bar_parent = cntr.append("svg");

        const opts = cntr.append("div").attr("class", "opts");

        let cur = 0;

        const ns = data.languages[0].implementations[0].paradigms[0].bench.map(
            (i) => i.n
        );

        for (let i = 0; i < ns.length; i++) {
            const n = ns[i];
            const nf = d3.format("~s")(n);
            const radio = opts.append("div");

            const input = radio
                .append("input")
                .attr("type", "radio")
                .attr("id", `radio_${i}`)
                .attr("name", "n_radio")
                .attr("value", n);

            radio.append("label").attr("for", `radio_${i}`).text(nf);

            input.on("click", () => {
                const all = all_times(data, i, choice);
                cur = i;
                update_bar_plot_horizontal(svg_bar, x_bar, y_bar, dim, all);
            });
        }

        d3.select("#radio_0").property("checked", "true");

        let choice = data.languages.reduce(
            (acc0, language) =>
                acc0 +
                language.implementations.reduce(
                    (acc1, implementation) =>
                        acc1 + implementation.paradigms.length,
                    0
                ),
            0
        );

        const all = all_times(data, cur, choice);

        const slct = opts.append("input");

        slct.attr("type", "number")
            .attr("min", 1)
            .attr("max", all.length)
            .attr("value", all.length)
            .attr("class", "number-input");

        slct.on("change", function (d) {
            choice = d3.select(this).property("value");
            const all = all_times(data, cur, choice);
            update_bar_plot_horizontal(svg_bar, x_bar, y_bar, dim, all);
        });

        const labels = {
            axis: {
                x: "Time (s)",
            },
            title: "Comparative performance",
        };

        const dim = new Dim();
        dim.margin.left = 170;

        const [svg_bar, x_bar, y_bar] = bar_plot_horizontal(
            svg_bar_parent,
            dim,
            all,
            labels
        );
    }
});
